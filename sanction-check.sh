#!/bin/bash

set -eu
if [ -z ${1:-} ]
then
    echo "Call with 'spec' or other host/group as target"
    exit 1
fi
if [ -z ${2:-} ]
then
    echo "Pass sanction list as 2nd argument"
    exit 1
fi
if [ -f ${2:-} ]
then
    echo "Sanction list '$2' not found"
    exit 1
fi

ansible-playbook \
    --extra-vars "SANCTION_LIST=$2" \
    --verbose \
    --limit "$1" \
    --inventory inventories/default \
    playbooks/sanctionlist-check.yml
