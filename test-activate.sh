#!/bin/sh

# Script to fully configure the taler-ops "test" exchange.

taler-exchange-offline \
    -c test-offline.conf \
    download sign upload


taler-exchange-offline \
    -c test-offline.conf \
    wire-fee now iban CHF:0 CHF:0 \
    global-fee now CHF:0 CHF:0 CHF:0 "1 d" "1 year" 1000 \
    enable-account payto://iban/CH6808573105529100001?receiver-name=Taler+Operations+AG \
      credit-restriction regex 'payto://iban/CH.*' "Swiss bank accounts only" '{}' \
      debit-restriction regex 'payto://iban/CH.*' "Swiss bank accounts only" '{}' \
    enable-auditor P6B7ZS7Y1Y12S0VP0PAJ1GQGSHW8RE4NSBTP8PR254J18SK24MH0 https://auditor.taler-ops.ch/ "Taler Operations AG" \
    upload