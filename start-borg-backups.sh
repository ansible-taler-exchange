#!/bin/bash

set -eu

if [ -z ${BORG_PASSPHRASE:-} ]
then
    echo "You need to set the BORG_PASSPHRASE in your environment before running this script!"
    exit 1
fi

ansible-playbook \
  --verbose \
  --extra-vars BORG_PASSPHRASE="$BORG_PASSPHRASE" \
  --inventory inventories/default \
  --limit "${1:-spec}" \
  playbooks/borg-start.yml

exit 0
