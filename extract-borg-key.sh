#!/bin/bash

set -eu

ansible-playbook \
    --inventory inventories/default \
    --limit "${1:-spec}" \
    --user root \
    playbooks/borg-ssh-export.yml
cat borg.pub/*/root/.ssh/borg.pub
rm -rf borg.pub/
exit 0
