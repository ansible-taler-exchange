#!/bin/sh
set -eu

if [ -z ${1:-} ]
then
    echo "Call with 'spec' or another host/group to select target"
    exit 1
fi

ansible-playbook \
    -vv \
    --inventory inventories/default \
    --limit "$1" \
    playbooks/setup.yml

exit 0
