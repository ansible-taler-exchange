---
- name: Get the list of services
  service_facts:

- name: Ensure libeufin-nexus service is stopped before we upgrade
  systemd:
    name: libeufin-nexus.target
    state: stopped
    enabled: false
  when: "'libeufin-nexus.target' in services"

- name: Ensure libeufin-nexus-httpd service is stopped before we upgrade
  service:
    name: libeufin-nexus-httpd.service
    state: stopped
    enabled: false
  when: "'libeufin-nexus-httpd.service' in services"

- name: Install libeufin-nexus package
  apt:
    name:
      - libeufin-nexus
    state: latest
  when: ansible_os_family == 'Debian'

- name: Ensure libeufin config dir exists from installation
  file:
    path: "/etc/libeufin"
    state: directory
    mode: "0755"
    owner: root
    group: root

# FIXME: is this needed or always there in Ansible?
- name: Ensure Ansible facts directory dir exists
  file:
    path: "/etc/ansible/facts.d/"
    state: directory
    mode: "0700"
    owner: root
    group: root

- name: Libeufin-nexus access secret setup
  ansible.builtin.command:
    argv:
      - setup-secret-fact
      - /etc/ansible/facts.d/libeufin-nexus-access-token.fact
      - "secret-token:"
    creates: /etc/ansible/facts.d/libeufin-nexus-access-token.fact

- name: Libeufin-nexus force ansible to regather just created fact(s)
  ansible.builtin.setup:

- name: Place libeufin-nexus config
  ansible.builtin.template:
    src: templates/etc/libeufin/libeufin-nexus.conf.j2
    dest: "/etc/libeufin/libeufin-nexus.conf"
    owner: root
    group: root
    mode: "0644"

- name: Place libeufin-nexus EBICS config
  ansible.builtin.template:
    src: templates/etc/libeufin/libeufin-nexus-ebics.conf.j2
    dest: "/etc/libeufin/libeufin-nexus-ebics.conf"
    owner: root
    group: libeufin-nexus
    mode: "0640"
  when: use_ebics

- name: Setup libeufin database
  ansible.builtin.command:
    cmd: libeufin-dbconfig --only-nexus
# FIXME: pass "--bank-config=/etc/libeufin/libeufin-nexus.conf" once libeufin 0.14.x is out!
    chdir: /tmp

- name: Show vars
  ansible.builtin.setup:

# FIXME: this step currently fails with pofi, seems command wants
# extra arguments to do PDF letter generation?
- name: EBICS setup
  become: true
  become_user: libeufin-nexus
  ansible.builtin.command:
    cmd: libeufin-nexus ebics-setup
  when: use_ebics

- name: Ensure libeufin-nexus target is enabled and started
  service:
    daemon_reload: true
    name: libeufin-nexus.target
    state: started
    enabled: true
  when: use_ebics

- name: Ensure libeufin-nexus-httpd service is enabled and started
  service:
    daemon_reload: true
    name: libeufin-nexus-httpd.service
    state: started
    enabled: true
  when: not use_ebics

- name: Place login script for libeufin-nexus-import technical user
  ansible.builtin.copy:
    src: usr/local/bin/libeufin-nexus-import.sh
    dest: "/usr/local/bin/libeufin-nexus-import.sh"
    owner: root
    group: root
    mode: "0755"

- name: Place login script for libeufin-nexus-export technical user
  ansible.builtin.copy:
    src: usr/local/bin/libeufin-nexus-export.sh
    dest: "/usr/local/bin/libeufin-nexus-export.sh"
    owner: root
    group: root
    mode: "0755"

- name: Ensure group for libeufin-nexus-import exists
  group:
    name: libeufin-nexus-import
  when: not use_ebics

- name: Ensure group for libeufin-nexus-export exists
  group:
    name: libeufin-nexus-export
  when: not use_ebics

- name: Ensure technical user for libeufin-nexus import exists
  user:
    name: libeufin-nexus-import
    group: libeufin-nexus-import
    shell: /usr/local/bin/libeufin-nexus-import.sh
    password: '!'
  when: not use_ebics

- name: Ensure technical user for libeufin-nexus export exists
  user:
    name: libeufin-nexus-export
    group: libeufin-nexus-export
    shell: /usr/local/bin/libeufin-nexus-export.sh
    password: '!'
  when: not use_ebics

- name: Grant sudo rights to login script for importer
  ansible.builtin.copy:
    src: etc/sudoers.d/libeufin-nexus-import
    dest: "/etc/sudoers.d/libeufin-nexus-import"
    owner: root
    group: root
    mode: "0644"
  when: not use_ebics

- name: Grant sudo rights to login script for exporter
  ansible.builtin.copy:
    src: etc/sudoers.d/libeufin-nexus-export
    dest: "/etc/sudoers.d/libeufin-nexus-export"
    owner: root
    group: root
    mode: "0644"
  when: not use_ebics

- name: Ensure .ssh dir exists for libeufin-nexus-import user
  file:
    path: "/home/libeufin-nexus-import/.ssh/"
    state: directory
    owner: libeufin-nexus-import
    group: libeufin-nexus-import
    mode: "0755"
  when: not use_ebics

- name: Ensure .ssh dir exists for libeufin-nexus-export user
  file:
    path: "/home/libeufin-nexus-export/.ssh/"
    state: directory
    owner: libeufin-nexus-export
    group: libeufin-nexus-export
    mode: "0755"
  when: not use_ebics

- name: Allow technical users access to import acocunt.
  ansible.builtin.copy:
    src: home/libeufin-nexus-import/.ssh/authorized_keys
    dest: "/home/libeufin-nexus-import/.ssh/authorized_keys"
    owner: libeufin-nexus-import
    group: libeufin-nexus-import
    mode: "0644"
  when: not use_ebics

- name: Allow technical users access to export acocunt.
  ansible.builtin.copy:
    src: home/libeufin-nexus-export/.ssh/authorized_keys
    dest: "/home/libeufin-nexus-export/.ssh/authorized_keys"
    owner: libeufin-nexus-export
    group: libeufin-nexus-export
    mode: "0644"
  when: not use_ebics
