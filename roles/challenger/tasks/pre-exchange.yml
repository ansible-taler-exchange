---
- name: Populate service facts
  service_facts:

- name: Ensure SMS challenger service is stopped before we upgrade
  ansible.builtin.systemd_service:
    name: sms-challenger
    state: stopped
    enabled: false
  when: "'SMS Challenger backend' in services"

- name: Ensure email challenger service is stopped before we upgrade
  ansible.builtin.systemd_service:
    name: email-challenger
    state: stopped
    enabled: false
  when: "'Email Challenger backend' in services"

- name: Ensure postal challenger service is stopped before we upgrade
  ansible.builtin.systemd_service:
    name: postal-challenger
    state: stopped
    enabled: false
  when: "'Postal Challenger backend' in services"

- name: Install Challenger packages
  apt:
    name:
      - challenger-httpd
    state: latest
  when: ansible_os_family == 'Debian'

- name: Ensure group "challenger-sms" exists
  ansible.builtin.group:
    name: challenger-sms
    state: present

- name: Ensure user "challenger-sms" exists
  ansible.builtin.user:
    name: challenger-sms
    group: challenger-sms
    password: !
    system: true
    state: present

- name: Ensure group "challenger-postal" exists
  ansible.builtin.group:
    name: challenger-postal
    state: present

- name: Ensure user "challenger-postal" exists
  ansible.builtin.user:
    name: challenger-postal
    group: challenger-postal
    password: !
    system: true
    state: present

- name: Ensure group "challenger-email" exists
  ansible.builtin.group:
    name: challenger-email
    state: present

- name: Ensure user "challenger-email" exists
  ansible.builtin.user:
    name: challenger-email
    group: challenger-email
    password: !
    system: true
    state: present

- name: Ensure /var/run/challenger-email/ directory exists
  file:
    path: "/var/run/challenger-email/"
    state: directory
    owner: challenger-email
    group: www-data
    mode: "0755"

- name: Ensure /var/run/challenger-sms/ directory exists
  file:
    path: "/var/run/challenger-sms/"
    state: directory
    owner: challenger-sms
    group: www-data
    mode: "0755"

- name: Ensure /var/run/challenger-postal/ directory exists
  file:
    path: "/var/run/challenger-postal/"
    state: directory
    owner: challenger-postal
    group: www-data
    mode: "0755"

- name: Ensure Ansible facts directory exists
  file:
    path: "/etc/ansible/facts.d/"
    state: directory
    owner: root
    group: root
    mode: "0700"

- name: Ensure /etc/taler-exchange/secrets directory exists
  file:
    path: "/etc/taler-exchange/secrets"
    state: directory
    owner: root
    group: root
    mode: "0755"

- name: Secret setup for sms-challenger
  when: not local_facts['sms-challenger-client-secret'] is defined
  ansible.builtin.command:
    argv:
      - setup-secret-fact
      - /etc/ansible/facts.d/sms-challenger-client-secret.fact
      - "secret-token:"
    creates: /etc/ansible/facts.d/sms-challenger-client-secret.fact

- name: Secret setup for email-challenger
  ansible.builtin.command:
    argv:
      - setup-secret-fact
      - /etc/ansible/facts.d/email-challenger-client-secret.fact
      - "secret-token:"
    creates: /etc/ansible/facts.d/email-challenger-client-secret.fact

- name: Secret setup for postal-challenger
  ansible.builtin.command:
    argv:
      - setup-secret-fact
      - /etc/ansible/facts.d/postal-challenger-client-secret.fact
      - "secret-token:"
    creates: /etc/ansible/facts.d/postal-challenger-client-secret.fact

- name: Force ansible to regather just created fact(s) about challenger
  ansible.builtin.setup:
    filter:
      - 'sms-challenger-client-secret'
      - 'email-challenger-client-secret'
      - 'postal-challenger-client-secret'

- name: Place SMS challenger config
  ansible.builtin.template:
    src: templates/etc/challenger/challenger-sms.conf.j2
    dest: "/etc/challenger/challenger-sms.conf"
    owner: root
    group: challenger-sms
    mode: "0640"

- name: Place Postal challenger config
  ansible.builtin.template:
    src: templates/etc/challenger/challenger-postal.conf.j2
    dest: "/etc/challenger/challenger-postal.conf"
    owner: root
    group: challenger-postal
    mode: "0640"

- name: Place email challenger config
  ansible.builtin.template:
    src: templates/etc/challenger/challenger-email.conf.j2
    dest: "/etc/challenger/challenger-email.conf"
    owner: root
    group: challenger-email
    mode: "0640"

- name: Place SMS challenger environment data
  ansible.builtin.template:
    src: templates/etc/challenger/sms-challenger.env.j2
    dest: /etc/challenger/sms-challenger.env
    owner: root
    group: challenger-sms
    mode: "0640"

- name: Place postal challenger environment data
  ansible.builtin.template:
    src: templates/etc/challenger/postal-challenger.env.j2
    dest: /etc/challenger/postal-challenger.env
    owner: root
    group: challenger-postal
    mode: "0640"

- name: Setup SMS Challenger database
  ansible.builtin.command:
    cmd: challenger-dbconfig -c /etc/challenger/challenger-sms.conf -u challenger-sms -n challenger-sms
    chdir: /tmp

- name: Setup Postal Challenger database
  ansible.builtin.command:
    cmd: challenger-dbconfig -c /etc/challenger/challenger-postal.conf -u challenger-postal -n challenger-postal
    chdir: /tmp

- name: Setup email Challenger database
  ansible.builtin.command:
    cmd: challenger-dbconfig -c /etc/challenger/challenger-email.conf -u challenger-email -n challenger-email
    chdir: /tmp

- name: Force ansible to regather newly created fact(s) about sms-challenger
  ansible.builtin.setup:

- name: Setup SMS Challenger exchange account
  ansible.builtin.command:
    argv:
      - setup-challenger-client-id-fact
      - /etc/ansible/facts.d/sms-challenger-client-id.fact
      - challenger-sms
      - /etc/challenger/challenger-sms.conf
      - "{{ ansible_local['sms-challenger-client-secret'] }}"
      - "{{ EXCHANGE_BASE_URL }}sms-proof/email-challenger"
    creates: /etc/ansible/facts.d/sms-challenger-client-id.fact

- name: Setup Email Challenger exchange account
  ansible.builtin.command:
    argv:
      - setup-challenger-client-id-fact
      - /etc/ansible/facts.d/email-challenger-client-id.fact
      - challenger-email
      - /etc/challenger/challenger-email.conf
      - "{{ ansible_local['email-challenger-client-secret'] }}"
      - "{{ EXCHANGE_BASE_URL }}kyc-proof/email-challenger"
    creates: /etc/ansible/facts.d/email-challenger-client-id.fact

- name: Setup Postal Challenger exchange account
  ansible.builtin.command:
    argv:
      - setup-challenger-client-id-fact
      - /etc/ansible/facts.d/postal-challenger-client-id.fact
      - challenger-postal
      - /etc/challenger/challenger-postal.conf
      - "{{ ansible_local['postal-challenger-client-secret'] }}"
      - "{{ EXCHANGE_BASE_URL }}kyc-proof/postal-challenger"
    creates: /etc/ansible/facts.d/postal-challenger-client-id.fact

- name: Force ansible to regather fact(s) just created about sms-challenger
  ansible.builtin.setup:

- name: Place sms-challenger systemd service file
  copy:
    src: etc/systemd/system/sms-challenger-httpd.service
    dest: /etc/systemd/system/sms-challenger-httpd.service
    owner: root
    group: root
    mode: "0700"

- name: Place postal-challenger systemd service file
  copy:
    src: etc/systemd/system/postal-challenger-httpd.service
    dest: /etc/systemd/system/postal-challenger-httpd.service
    owner: root
    group: root
    mode: "0700"

- name: Place email-challenger systemd service file
  copy:
    src: etc/systemd/system/email-challenger-httpd.service
    dest: /etc/systemd/system/email-challenger-httpd.service
    mode: "0700"

- name: Ensure SMS challenger service is enabled and started
  ansible.builtin.systemd_service:
    daemon_reload: true
    name: sms-challenger-httpd
    state: started
    enabled: true

- name: Ensure email challenger service is enabled and started
  ansible.builtin.systemd_service:
    name: email-challenger-httpd
    state: started
    enabled: true

- name: Ensure postal challenger service is enabled and started
  ansible.builtin.systemd_service:
    name: postal-challenger-httpd
    state: started
    enabled: true

- name: Place SMS challenger HTTP Nginx configuration
  ansible.builtin.template:
    src: templates/etc/nginx/sites-available/sms-challenger-http.conf.j2
    dest: /etc/nginx/sites-available/sms-challenger-http.conf
    owner: root
    group: root
    mode: "0644"

- name: Place SMS challenger Nginx configuration
  ansible.builtin.template:
    src: templates/etc/nginx/sites-available/sms-challenger-nginx.conf.j2
    dest: /etc/nginx/sites-available/sms-challenger-nginx.conf
    owner: root
    group: root
    mode: "0644"

- name: Enable SMS challenger HTTP reverse proxy configuration
  file:
    src: /etc/nginx/sites-available/sms-challenger-http.conf
    dest: /etc/nginx/sites-enabled/sms-challenger-http.conf
    state: link
  notify: Restart nginx

- name: Place email challenger HTTP Nginx configuration
  ansible.builtin.template:
    src: templates/etc/nginx/sites-available/email-challenger-http.conf.j2
    dest: /etc/nginx/sites-available/email-challenger-http.conf
    owner: root
    group: root
    mode: "0644"

- name: Place email challenger Nginx configuration
  ansible.builtin.template:
    src: templates/etc/nginx/sites-available/email-challenger-nginx.conf.j2
    dest: /etc/nginx/sites-available/email-challenger-nginx.conf
    owner: root
    group: root
    mode: "0644"

- name: Enable email challenger HTTP reverse proxy configuration
  file:
    src: /etc/nginx/sites-available/email-challenger-http.conf
    dest: /etc/nginx/sites-enabled/email-challenger-http.conf
    state: link
  notify: Restart nginx

- name: Place postal challenger HTTP configuration
  ansible.builtin.template:
    src: templates/etc/nginx/sites-available/postal-challenger-http.conf.j2
    dest: /etc/nginx/sites-available/postal-challenger-http.conf
    owner: root
    group: root
    mode: "0644"

- name: Place postal challenger Nginx configuration
  ansible.builtin.template:
    src: templates/etc/nginx/sites-available/postal-challenger-nginx.conf.j2
    dest: /etc/nginx/sites-available/postal-challenger-nginx.conf
    owner: root
    group: root
    mode: "0644"

- name: Enable postal challenger HTTP reverse proxy configuration
  file:
    src: /etc/nginx/sites-available/postal-challenger-http.conf
    dest: /etc/nginx/sites-enabled/postal-challenger-http.conf
    state: link
  notify: Restart nginx

# We need to make sure that our handler notifies nginx to restart NOW
- name: Flush handlers
  meta: flush_handlers

- name: Secure the SMS challenger site with Letsencrypt
  ansible.builtin.include_role:
    name: geerlingguy.certbot
  vars:
    certbot_install_method: package
    certbot_auto_renew: true
    certbot_auto_renew_user: "{{ ansible_user | default(lookup('env', 'USER')) }}"
    certbot_auto_renew_hour: "11"
    certbot_auto_renew_minute: "11"
    certbot_auto_renew_options: "--quiet"
    certbot_create_method: webroot
    certbot_create_if_missing: true
    certbot_create_extra_args:
    certbot_hsts: false
    certbot_testmode: false
    certbot_admin_email: "admin@{{ DOMAIN_NAME }}"
    certbot_keep_updated: true
    certbot_script: letsencrypt
    certbot_certs:
      - webroot: "/var/www/letsencrypt/sms.challenger.{{ DOMAIN_NAME }}"
        domains:
          - "sms.challenger.{{ DOMAIN_NAME }}"

- name: Secure the EMAIL challenger site with Letsencrypt
  ansible.builtin.include_role:
    name: geerlingguy.certbot
  vars:
    certbot_install_method: package
    certbot_auto_renew: true
    certbot_auto_renew_user: "{{ ansible_user | default(lookup('env', 'USER')) }}"
    certbot_auto_renew_hour: "11"
    certbot_auto_renew_minute: "11"
    certbot_auto_renew_options: "--quiet"
    certbot_create_method: webroot
    certbot_create_if_missing: true
    certbot_create_extra_args:
    certbot_hsts: false
    certbot_testmode: false
    certbot_admin_email: "admin@{{ DOMAIN_NAME }}"
    certbot_keep_updated: true
    certbot_script: letsencrypt
    certbot_certs:
      - webroot: "/var/www/letsencrypt/email.challenger.{{ DOMAIN_NAME }}"
        domains:
          - "email.challenger.{{ DOMAIN_NAME }}"


- name: Secure the POSTAL challenger site with Letsencrypt
  ansible.builtin.include_role:
    name: geerlingguy.certbot
  vars:
    certbot_install_method: package
    certbot_auto_renew: true
    certbot_auto_renew_user: "{{ ansible_user | default(lookup('env', 'USER')) }}"
    certbot_auto_renew_hour: "11"
    certbot_auto_renew_minute: "11"
    certbot_auto_renew_options: "--quiet"
    certbot_create_method: webroot
    certbot_create_if_missing: true
    certbot_create_extra_args:
    certbot_hsts: false
    certbot_testmode: false
    certbot_admin_postal: "admin@{{ DOMAIN_NAME }}"
    certbot_keep_updated: true
    certbot_script: letsencrypt
    certbot_certs:
      - webroot: "/var/www/letsencrypt/postal.challenger.{{ DOMAIN_NAME }}"
        domains:
          - "postal.challenger.{{ DOMAIN_NAME }}"

- name: Enable SMS challenger reverse proxy configuration
  file:
    src: /etc/nginx/sites-available/sms-challenger-nginx.conf
    dest: /etc/nginx/sites-enabled/sms-challenger-nginx.conf
    state: link
  notify: Restart nginx

- name: Enable email challenger reverse proxy configuration
  file:
    src: /etc/nginx/sites-available/email-challenger-nginx.conf
    dest: /etc/nginx/sites-enabled/email-challenger-nginx.conf
    state: link
  notify: Restart nginx

- name: Enable postal challenger reverse proxy configuration
  file:
    src: /etc/nginx/sites-available/postal-challenger-nginx.conf
    dest: /etc/nginx/sites-enabled/postal-challenger-nginx.conf
    state: link
  notify: Restart nginx
