#!/bin/bash
set -exuo pipefail

# Build our image
podman build -f Containerfile -t ansible-taler-test

# Run in background (-d) with systemd init
podman run \
	--rm \
	--name ansible-taler-test \
	-p 127.0.0.1:8022:22 \
	--systemd=always \
	-d localhost/ansible-taler-test sh -c "exec /usr/sbin/init --show-status"

# Print to log that container is running
podman ps

# Clear out fingerprint from any past runs
ssh-keygen -f "$HOME/.ssh/known_hosts" -R "[127.0.0.1]:8022"

# Run our playbook(s)
ansible-playbook \
    --verbose \
    -i inventories/default \
    -l "podman-localhost" \
    --user root \
    playbooks/setup.yml
