#!/bin/bash

set -eu

if [ -z ${PIXEL_BORG_KEY:-} ]
then
    echo "You need to set the PIXEL_BORG_KEY in your environment before running this script (see admin-log/pixel/03-borg.txt)"
    exit 1
fi
ansible-playbook \
    --extra-vars PIXEL_BORG_KEY="$PIXEL_BORG_KEY" \
    --inventory inventories/default \
    --limit "${1:-spec}" \
    --user root \
    playbooks/pixel_borg.yml
mv borg-repokey/*/home/borg/borg-repo.key .
rm -rf borg-repokey/
echo "Make sure to back up the borg-repo.key to admin-log/pixel/borg-repo.key"
exit 0
