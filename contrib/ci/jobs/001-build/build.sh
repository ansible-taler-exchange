#!/bin/bash
set -exuo pipefail

#### WARNING: THIS SCRIPT IS INTENED TO BE RUN INSIDE OF A CONTAINER


# Print some debug info
id ; cat /proc/self/uid_map ; mount | grep cgroup || true

# Hack to make podman adapt to being nested
rm -f /etc/containers/storage.conf

# Build our image
podman build -f Containerfile -t ansible-taler-test

# Run in background (-d) with systemd init
podman run \
	--privileged \
	--tmpfs /sys \
	--rm \
	--name ansible-taler-test \
	-d localhost/ansible-taler-test sh -c "id ; cat /proc/self/uid_map ; mount | grep cgroup; exec /usr/sbin/init --show-status"  

# Print to log that container is running
podman ps

# TOFU SSH host keys (so we don't get user prompt)
echo "StrictHostKeyChecking=accept-new" > ~/.ssh/config

# Run our playbook(s)
# NOTE: Trailing comma is correct (and required) in agument for -i flag
ansible-playbook --verbose -i 127.0.0.1:22, --user root playbooks/setup.yml

echo -e '
         #############################
         #############################
	 #############################
	 ###### Setup finished. ######
	 ## Launching services now! ##
	 #############################
	 #############################
	 #############################'

ansible-playbook --verbose -i 127.0.0.1:22, --user root playbooks/start.yml
